import requests
import json


def create_personal_info(id, fullname, country, household, current_loc, current_rec, bank):
    payload = {
        '$class': 'personalnetwork.personalInfo',
        'personalId': id,
        'personalFullname': fullname,
        'personalCountry': country,
        'personalHouseHold': household,
        'personalCurrentLoc': current_loc,
        'personalRecStat': current_rec,
        'bank': bank
    }

    res = requests.post("http://pycon.vn:3000/api/personalInfo", data=json.dumps(payload))
    return res.status_code


def get_personal_info(id):
    res = requests.get("http://pycon.vn:3000/api/personalInfo?filter=%7B%22personalId%22%3A%20%22{0}%22%7D".format(id))
    return res


if __name__ == "__main__":
    while True:
        print ("1. Get all personal list")
        print ("2. Create a new personal record")
        print ("x. Exit")
        options = raw_input("Choose your options: ")
        if options == '1':
            personal_id = raw_input("Personal ID: ")
            print (get_personal_info(personal_id))
        if options == '2':
            id = raw_input("* Personal ID: ")
            fullname = raw_input("* Full name: ")
            country = raw_input("* Country: ")
            household = raw_input("* House Hold: ")
            current_loc = raw_input("* Current Location: ")
            current_rec = raw_input("* Current bank record: ")
            bank = raw_input("* Bank: ")

            res_status = create_personal_info(id, fullname, country, household, current_loc, current_rec, bank)
            print ("-> Status: {0}".format(res_status))
        if options == 'x':
            break
